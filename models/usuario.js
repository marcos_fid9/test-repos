var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const mongooseUniqueValidator = require('mongoose-unique-validator');

const saltRounds = 10;
const Token = require('../models/token');
const mailer = require('../mailer/mailer');

const validateEmail = function(email){
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
}
var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true,'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true,'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail,'Por favor ingrese un email valido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true,'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

usuarioSchema.plugin(mongooseUniqueValidator,{message: 'el {PATH} ya existe con otro usuario'});

usuarioSchema.pre('save',function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password,saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password,this.password);
}

usuarioSchema.methods.reservar = function(biciId,desde,hasta,cb){
    var reserva = new Reserva({usuario:this._id,bicicleta: biciId,desde:desde,hasta: hasta});
    console.log(reserva); 
    reserva.save(cb);
}

usuarioSchema.methods.resetPassword = function (callback) {
    const token = new Token({ _userid: this._id, token: crypto.ramdomBytes(16).toString('hex') });
    const email_dest = this.email;

    token.save(function(err){
        if(err) {return console.log(err.message);}
        const mailOption = {
            from : 'no-reply@unknow.com',
            to: email_dest,
            subject: 'Verificacion de cuenta',
            text: 'Hola, \n\n Por favor, para verificar su cuenta haga click en el siguiente enlace \n '+'http://localhost:3000'+'\/token/confirmation\/'+token.token+'\n'
        }

        Mailer.sendMail(mailOption,function(err){
            if(err) {return console.log(err.message);}
            console.log('A verification email has been sent to '+ email_dest+'.');
        });
        callback(null)
    });   
}

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition,callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or: [{'googleId': condition.id},{'email': condition.emails[0].value}]},(err,result)=>{
            if(result){
                callback(err,result);
            }else{
                console.log('_____________CONDITION__________')
                console.log(condition);
                let values = {};
                values.googleId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = condition._json.etag;
                console.log('___________VALUES_______________');
                console.log(values);
                self.create(values,(err,result)=>{
                    if(err){console.log(err);}
                    return callback(err,result);
                })
            }
        })
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition,callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or: [{'facebookId': condition.id},{'email': condition.emails[0].value}]},(err,result)=>{
            if(result){
                callback(err,result);
            }else{
                console.log('_____________CONDITION__________')
                console.log(condition);
                let values = {};
                values.facebookId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = crypto.ramdomBytes(16).toString('hex');
                console.log('___________VALUES_______________');
                console.log(values);
                self.create(values,(err,result)=>{
                    if(err){console.log(err);}
                    return callback(err,result);
                })
            }
        })
};

usuarioSchema.methods.enviar_email_forgotPassword = function (callback) {
    const email_dest = this.email;
    const crypt_id = Security.encrypt(this._id.toString());
    const mailOption = {
        from: 'no-reply@unknow.com',
        to: email_dest,
        subject: 'Recuperación de contraseña',
        text: 'Hola, \n\n Por favor, para recuperar su contraseña haga click en el siguiente enlace \n ' + 'http://localhost:3000/resetPassword/' + crypt_id + '\n'
    }

    Mailer.sendMail(mailOption,function(err){
        if(err){return console.log(err.message);}
        console.log('A verification email has been sent to ' + email_dest + '.');
    });
}


module.exports = mongoose.model('Ususario',usuarioSchema);