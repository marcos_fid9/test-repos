var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
var mongoose = require('mongoose');

describe('Bicicleta API',() => {
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,{useNewUrlParser:true});
        const db = mongoose.connection;
        db.on('error',console.error.bind(console,'connection error'));
        db.once('open',function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({},function(err,success){
            if(err)console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /',() =>{
        it('Status 200',() => {
            expect(Bicicleta.allBicis.length).toBe(1);
            var a = new Bicicleta({"code":10,"color":"negro","modelo":"Urbana","lat":-34,"lng":-54});
            Bicicleta.add(a);
            request.get('http://localhost:3000/api/bicicletas', function(error,response,body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST BICICLETAS / create',() => {
  
        it('STATUS 200',(done) => {
            var headers = {'content-type':'application/json'};
            var aBici = '{"code":10,"color":"rojo","modelo":"Urbana","lat":-34,"lng":-54}';
            request.post({
                headers: headers,
                url:'http://localhost:3000/api/bicicletas/create',
                body: aBici
            },function(error, response, body){
                if(error)console.log('error!!');
                expect(response.statusCode).toBe(200);
                //console.log(body.color);
                done();
            });
        });
       
    });

    
});

